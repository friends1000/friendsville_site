// BUFF -----------
import { contentTypes, linkTypes } from './index'


export default {
    id: 'buff',
    type: contentTypes.music,
    tag: null,
    title: 'Buff',
    description: `
        Summer's just around the corner, what's a better way to start than by feeling yourself?
        Catch a vibe with the team, we're staying Buff all 2019!

        This song is all about feeling yourself and catching a wave, grab yourself a cold water, and go get it in!
    `,
    image: require('../asset/img/releases/buff_cover.jpg'),

    release_date: new Date('2019-04-19'),

    links: {
        /*
        [linkTypes.spotify]: {
            link: 'https://open.spotify.com/track/1df0yPnNImWxqWjvYjZLQG',
            embed: 'https://open.spotify.com/embed/track/1df0yPnNImWxqWjvYjZLQG',
            display: true,
        },
        [linkTypes.applemusic]: {
            link: 'https://itunes.apple.com/gb/album/stardust-single/1454487536',
            embed: 'https://embed.music.apple.com/gb/album/stardust-single/1454487536',
            display: true,
        },
        */
        
        [linkTypes.soundcloud]: {
            link: 'https://soundcloud.com/friends1000/buff',
            embed: 'https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/524670198%3Fsecret_token%3Ds-1mevI&color=%23f4ec18&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true',
            display: true,
            // release_date: new Date()
        },
        
    },


}