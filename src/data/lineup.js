// LINE UP -----------
import { contentTypes, linkTypes } from './index'


export default  [{
    
    id: 'line_up',
    type: contentTypes.project,
    tag: null,
    title: 'Line Up',
    description: `Line Up is a 5 track playlist celebrating the beauty in Friendsville, each song being personified by a contestant in a beauty pageant.

    Line Up has a warm, cheerful tone layered with smooth vocals. The project starts on an up-tempo, bounce, and transitions through to a suave swing. All the songs are thematically linked to their respective contestants, the production itself gives you a very strong idea of who the contestants are and Friends1000\'s unique interactions with them.
    
    The production mostly consists of synthy trap beats with a mellow undertone to all of their melodies.`,
    
    image: require('../asset/img/releases/lineup_cover_comp.jpeg'),
    release_date: new Date('2018-11-26'),
    links: {

        [linkTypes.soundcloud]: {
            link: 'https://soundcloud.com/friends1000/sets/line-up',
            embed: "https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/playlists/592456206&color=%23b278cf&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true",
            display: true
        },
    },

    connections: {
        contestants: {
            title: 'Contestants',
            items: [
                {
                    id: 'lineup_haley',                       
                },
                {
                    id: 'lineup_ocean',
                },
                {
                    id: 'lineup_roshelle',
                },
                {
                    id: 'lineup_nia',
                },
                {
                    id: 'lineup_yvonne',
                }
            ]
        },

        sponsor: {
            title: 'Sponsored By',
            items: [
                {
                    id: 'bella_bites'
                }
            ]
        }
    }
},

{
    onContentPage: false,
    id: 'lineup_haley',
    type: contentTypes.music,
    tag: null,
    title: 'Haley',
    description: `'Haley Haley, baby - that tan is looking tasty'
    
    A young, pretty little thing. Tanned and exotic, you wont regret taking a second to vibe.
    `,
    
    image: require('../asset/img/line_up/haley.png'),
    release_date: new Date('2018-11-26'),
    links: {
       

        [linkTypes.soundcloud]: {
            link: 'https://soundcloud.com/friends1000/guava-mixa?in=friends1000/sets/line-up',
            embed: "https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/492342861&color=%23b278cf&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true",
            display: true
        },
    },

    connections: {
        competed: {
            title: 'Competed In',
            items: [
                {
                    id: 'line_up'
                }
            ]
        }
    }
},

{
    onContentPage: false,
    id: 'lineup_ocean',
    type: contentTypes.music,
    tag: null,
    title: 'Ocean',
    description: `
    ''See Ocean, watch her dress unfold'

    Intelligent, mysterious, captivating. You'll want to keep her close, there are waves to be caught.`,
    
    image: require('../asset/img/line_up/ocean.png'),
    release_date: new Date('2018-11-26'),
    links: {

        [linkTypes.soundcloud]: {
            link: 'https://soundcloud.com/friends1000/ocean-mixa?in=friends1000/sets/line-up',
            embed: "https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/492388389&color=%23b278cf&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true",
            display: true
        },
    },

    connections: {
        competed: {
            title: 'Competed In',
            items: [
                {
                    id: 'line_up'
                }
            ]
        }
    }
},

{
    onContentPage: false,
    id: 'lineup_roshelle',
    type: contentTypes.music,
    tag: null,
    title: 'Roshelle',
    description: `'Looking hella pretty how you (ex)pect me not to stare?'

    Ohhh she fiesty. The spicy one of the group, she knows what she wants and she isn't afriad to chase it. Don't burn yourself!`,
    
    image: require('../asset/img/line_up/roshelle.png'),
    release_date: new Date('2018-11-26'),
    links: {

        [linkTypes.soundcloud]: {
            link: 'https://soundcloud.com/friends1000/roshelle-mixa?in=friends1000/sets/line-up',
            embed: "https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/497062998&color=%23b278cf&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true",
            display: true
        },
    },

    connections: {
        competed: {
            title: 'Competed In',
            items: [
                {
                    id: 'line_up'
                }
            ]
        }
    }
},
{
    onContentPage: false,
    id: 'lineup_nia',
    type: contentTypes.music,
    tag: null,
    title: 'Nia',
    description: `'Yeah, She luscious to the touch'

    Soothing, tender. She makes you too comfotable to move and then fixes a plate of what you needed. She's just that good.
    `,
    
    image: require('../asset/img/line_up/nia.png'),
    release_date: new Date('2018-11-26'),
    links: {

        [linkTypes.soundcloud]: {
            link: 'https://soundcloud.com/friends1000/nia-mixa?in=friends1000/sets/line-up',
            embed: "https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/493333392&color=%23b278cf&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true",
            display: true
        },
    },

    connections: {
        competed: {
            title: 'Competed In',
            items: [
                {
                    id: 'line_up'
                }
            ]
        }
    }
},
{
    onContentPage: false,
    id: 'lineup_yvonne',
    type: contentTypes.music,
    tag: null,
    title: 'Yvonne',
    description: `
    'Baby girl Yvonne, really 35 - but she feeling 21'

Wholesome is an understatement. The most expeienced in the group, she brings a whole new meaning to womanly.
    `,
    
    image: require('../asset/img/line_up/yvonne.png'),
    release_date: new Date('2018-11-26'),
    links: {

        [linkTypes.soundcloud]: {
            link: 'https://soundcloud.com/friends1000/yvonne-mixa?in=friends1000/sets/line-up',
            embed: "https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/492412170&color=%23b278cf&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true",
            display: true
        },
    },

    connections: {
        part_of: {
            title: 'Part Of',
            items: [
                {
                    id: 'line_up'
                }
            ]
        },
        
    }
},]

