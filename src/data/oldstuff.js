// Old stuff -----------
import { contentTypes, linkTypes } from './index'
import LazyLoad from 'react-lazyload'

export default  [// STAYCEE -----------

    {
        id: 'staycee',
        type: contentTypes.music,
        tag: null,
        title: 'Staycee',
        description: `Sweet ones.

        Conversational flows and warm days driving through the bits.
        `,
        
        image: require('../asset/img/releases/staycee_cover.png'),
        comp_img: require('../asset/img/releases/staycee_cover_comp.jpg'),

        links: {

            [linkTypes.soundcloud]: {
                link: 'https://soundcloud.com/friends1000/staycee',
                embed: 'https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/471580530&color=%2313d6de&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true',
                display: true
            },
            [linkTypes.spotify] :{
                link: 'https://open.spotify.com/track/5oMwdF9zXdfIAaT6kKmWb8?si=eKFhnjr8Qj2WwzUrBfgE4w',
                embed: 'https://open.spotify.com/embed/track/5oMwdF9zXdfIAaT6kKmWb8',
                
            },
            [linkTypes.applemusic] :{
                link: 'https://itunes.apple.com/us/album/staycee-single/1417829763',
                embed: 'https://embed.music.apple.com/us/album/bella-single/1435821680',
          
            }
        },

    },


    // SLIDE THROUGH -------
    {
        id: 'slide_through',
        type: contentTypes.music,
        tag: null,
        title: 'Slide Through',
        description: `This one brings back memories.
        
        First video we shot, one of the first times we showed people what we were doing. Really fun too - found a roller rink in Bury to film at.
        
        Also, how most of us met Dan (one of our team members). Was originally Sam's friend who he invited to the shoot and we kicked things off from there.
        
        Filmed by Naz Zaman:
        https://www.instagram.com/nxsimzxmxn/

        With his company Kinematic Visuals:
        https://www.instagram.com/kinematicvisuals/
        `,
        
        image: require('../asset/img/releases/slide_cover.jpg'),

        links: {

            [linkTypes.youtube]: {
                link: 'https://www.youtube.com/watch?v=c75NT-Mc3fw',
                embed: "https://www.youtube.com/embed/c75NT-Mc3fw",
                display: true
            },
        },

    },


    // MOONSHINE -------
    {
        id: 'moonshine',
        type: contentTypes.music,
        tag: null,
        title: 'Moonshine',
        description: `Our first ever release! 

        This one still gets love to this day. First song we made where we were like "maybe we should take music seriously". Fun times.`,
        
        image: require('../asset/img/releases/moonshine_cover.jpg'),

        links: {
            [linkTypes.soundcloud]: {
                link: 'https://soundcloud.com/friends1000/moonshine',
                embed: "https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/390573315&color=%2313d6de&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true",
                display: true
            }
        },

    }]

