export const SEO = {
    
    home: {
        title: 'Welcome to Friendsville',
        description: 'Welcome to Friendsville, Friends1000\'s creative world. ',
        metaname: 'Friends1000 friends 1000 friendsville music artist Manchester UK'
    },

    content: {
        title: 'F1K - Content',
        description: 'See our music, Friends1000\'s collection of content',
        metadata: 'music Friends1000 buff stardust hourglass'
    },

    residents: {
        title: 'F1K - Residents',
        description: '',
        metaname: 'Friends1000 Cazi Sammy Sama First Aid Residents Friendsville music' 
    },

    hotline: {
        title: 'F1K - Hotline',
        description: 'Get in touch with the team, Friends1000 is happy to hear from you!',
        metaname: 'Friends1000 Cazi Sammy Sama First Aid Residents Friendsville Contact Hotline '
    },
}