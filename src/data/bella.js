// Bella -----------
import { contentTypes, linkTypes } from './index'


export default  [{
    id: 'bella',
    type: contentTypes.music,
    tag: null,
    title: 'Bella',
    description: `You want happy vibes? You got happy vibes.
    
    Trippy with hint of saucy, this song was all about catching that summer drive vibe.

    Filmed by the talented Laurence:
     https://www.instagram.com/laurencesmedia/

    And his production team: Blue Shift Visuals:
    https://www.instagram.com/blueshiftvisuals/
    `,
    
    image: require('../asset/img/releases/bella_cover.png'),
    comp_img: require('../asset/img/releases/bella_cover_comp.jpg'),

    release_date: new Date('2018-09-16'),
    links: {

        [linkTypes.youtube]: {
            link: 'https://www.youtube.com/watch?v=TGGatM5KQG4&feature=youtu.be',
            embed: 'https://www.youtube.com/embed/TGGatM5KQG4',
            display: true
        },
        [linkTypes.soundcloud]: {
            link: 'https://soundcloud.com/friends1000/bella',
            embed: "https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/441512262&color=%23b278cf&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true",
            display: true
        },
        [linkTypes.applemusic]: {
            link: 'https://itunes.apple.com/gb/album/bella-single/1435821680',
            embed: 'https://embed.music.apple.com/us/album/bella-single/1435821680'
        },
        
        [linkTypes.spotify]: {
            link: 'https://open.spotify.com/track/7vit5u1xpE45jgsOGj3usD',
            embed: 'https://open.spotify.com/embed/track/7vit5u1xpE45jgsOGj3usD'
        }
    },

    connections: {
        involves: {
            title: 'Involves',
            items: [
                {
                    id: 'bella_bites'
                },
                {
                    id: 'friendsville_investment_group'
                }
            ]
        }
    }
},

{
    onContentPage: false,
    id: 'bella_bites',
    type: contentTypes.world,
    tag: null,
    title: 'Bella Bites',
    description: `Bringing you the, most nutricious, high in fructose, the FRIENDLIEST.\n\nBella Bites
    
    This limited edition cereal was used in filming Bella, a release from 2018 where the gang tried to sell this product to the hesitant Friendsville Investment Group.

    PS. We really made this cereal, we have some physical boxes back at the FriendZone (our studio).
    `,
    
    image: require('../asset/img/misc/bella_box_clean.png'),

    connections: {
        appearsIn: {
            title: 'Appears In',
            items: [
                {
                    id: 'bella'
                }
            ]
        },
        invested: {
            title: 'Invested By',
            items: [
                {
                    id: 'friendsville_investment_group',                       
                },
            ]
        }
    }
},

{
    onContentPage: false,
    id: 'friendsville_investment_group',
    type: contentTypes.world,
    tag: null,
    title: 'Friendsville Investment Group',
    description: `Friendsville's investment group, venture capitalists with a thirst for rulling it all.`,
    
    image: require('../asset/img/misc/friendsville_investment_group.jpg'),

    connections: {
        invested: {
            title: 'Invested In',
            items: [
                {
                    id: 'bella_bites',                       
                },
            ]
        },
        appearIn: {
            title: 'Appear In',
            items: [
                {
                    id: 'bella'
                }
            ]
        }
    }

}]

