// STAR DUST -----------
import { contentTypes, linkTypes } from './index'


export default {
    id: 'stardust',
    type: contentTypes.music,
    tag: null,
    title: 'Stardust',
    description: 'ENERGY on ENERGY.\nThis one kicks like a horse. \n\nBest enjoyed in a car or headphones - you want big speakers.',
    image: require('../asset/img/releases/stardust_cover.jpg'),
    release_date: new Date('2019-02-17'),

    links: {
        
        [linkTypes.spotify]: {
            link: 'https://open.spotify.com/track/1df0yPnNImWxqWjvYjZLQG',
            embed: 'https://open.spotify.com/embed/track/1df0yPnNImWxqWjvYjZLQG',
            display: true,
        },
        [linkTypes.applemusic]: {
            link: 'https://itunes.apple.com/gb/album/stardust-single/1454487536',
            embed: 'https://embed.music.apple.com/gb/album/stardust-single/1454487536',
            display: true,
        },
        [linkTypes.soundcloud]: {
            link: 'https://soundcloud.com/friends1000/stardust',
            embed: 'https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/532935342&color=%238013a2&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true',
            display: true,
            // release_date: new Date()
        },
    },
    
    connections: {

        during: {
            title: 'During',
            items: [
                {
                    id: 'hourglass',
                    name: 'Hourglass'
                },
            ]
        }
    }
}