// HOURGLASS -----------
import { contentTypes, linkTypes } from './index'


export default // HOURGLASS -------------
{
    id: 'hourglass',
    type: contentTypes.music,
    tag: null,
    title: 'Hourglass',
    release_date: new Date('2019-01-11'),

    description: `
    The climate of hip hop has always been stern. Unsurprisingly, being a genre born out of disenfranchisement. But recently, a surge towards the darker, grittier tones of Drill and Scream-Rap. When you take a step back, it does seem disproportionate.

    Combine that with one of one of the craziest years of the 20th Century; 2018. It certainly looks like a continuing trend. But with it, a fresh breeze, a new kind of attitude emerging in the urban scene. An undeniable, unrelenting optimism for the future. Enter Friends1000, Manchester's freshest inner city, alternative collective, with their debut single - 'Hourglass'.

    Hourglass’ hyperactive rhythm expresses itself in the most Friends1000 way possible. Making itself known with an undulating, soft, trap beat fitting the “small gathering turned into a party” tone. A party celebrating the new progressive attitude in hip hop. The modern man that grooms himself, is happy to be happy, and has a hands off demeanour with women -
    
    "2018 and we're grown, ain't no numbers on our belts - figure what you want yourself"
    
    Partnering up with videographer Theo Blackledge ( JME, Firsco - True To Me | Tremz - It Could Have Been ), Friends1000 display themselves visually with near-psychedelic patterns and colours as every verse plunges you deeper into your own personal listening party, with alcoholic drinks served.`,
    
    image: require('../asset/img/releases/hourglass_cover.jpg'),
    
    release_date: new Date('2019-01-11'),

    links: {
        
        [linkTypes.youtube]: {
            link: 'https://www.youtube.com/watch?v=5OGne8UvLnU',
            embed: 'https://www.youtube.com/embed/5OGne8UvLnU',
            display: true
        },
        [linkTypes.soundcloud]: {
            link: 'https://soundcloud.com/friends1000/hourglass',
            embed: 'https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/503592909&color=%23b61a4c&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true',
            display: true
        },
        [linkTypes.spotify]: {
            link: 'https://open.spotify.com/track/3FLiyyHbDV7kYWduDVVS5G',
            embed: 'https://open.spotify.com/embed/track/3FLiyyHbDV7kYWduDVVS5G',
            display: true
        },
        [linkTypes.applemusic]: {
            link: 'https://itunes.apple.com/us/album/hourglass/1448332660?i=1448332783',
            embed: 'https://embed.music.apple.com/us/album/hourglass-single/1448332660',
            //display: true
        }
    },
}