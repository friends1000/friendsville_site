
export const contentTypes = {
    music: 'music',
    project: 'project',
    sketch: 'sketch',
    world: 'world'
}   

export const linkTypes = {
    youtube: 'youtube',
    soundcloud: 'soundcloud',
    spotify: 'spotify',
    applemusic: 'applemusic'
}

export function getContentById(id) {
    for ( var i = 0; i < content.length; i ++) {
        if (content[i].id === id) {
            return content[i];
        }
    }

    return null;
}

export function getContentPageContent() {

    var returnArr = [];
    const today = new Date();

    for (var i = 0; i < content.length; i ++) {
        const item = content[i];
        if (item.onContentPage === false || item.release_date > today) {
            continue;
        }
        returnArr.push(content[i])
    }

    return returnArr;
    // order by release date
}


const buff      =   require('./buff').default
const starDust  =   require('./stardust').default
const hourglass =   require('./hourglass').default
const lineUp    =   require('./lineup').default
const bella     =   require('./bella').default
const oldStuff  =   require('./oldstuff').default


export const content = [

    // Singles ----------------

    buff,

    starDust,

    hourglass,

    // LINE UP -------------------
    ...lineUp,

    // BELLA -------------
    ...bella,
    
    ...oldStuff,
]



export const residents = [
    {
        id: 'resident_cazi',
        name: 'Cazi',
        quote: 'She throw it back fast so I call her arse sprint',
        image: require('../asset/img/illustrations/cazi.png'),
        description: 'The logical one, engineer. Looks like he exfoliates',
        data: [
            { key: 'Mixing', value: 81 },
            { key: 'Sauce', value: 99 },
            { key: 'Beard', value: 75 },
            { key: 'Bars', value: 83 },
        ]
    },
    {
        id: 'resident_first_aid',
        name: 'First Aid',
        image: require('../asset/img/illustrations/first_aid.png'),
        quote: 'I\'m smelling buff, she looking buff',
        description: 'The wavy one, designer. Backbone of the Friendsville aesthetic.',
        data: [
            { key: 'Photoshop', value: 85 },
            { key: 'Sauce', value: 99 },
            { key: 'Smash Bros', value: 89 },
            { key: 'Creativity', value: 84 },
        ]
    },
    {
        id: 'resident_sammy_sama',
        name: 'Sammy Sama',
        image: require('../asset/img/illustrations/sammy_sama.png'),
        quote: 'Making money rain on her toes',
        description: 'The happy one, model. Undying optimisim and relentlessly pushing forwards',
        data: [
            { key: 'Flow', value: 89 },
            { key: 'Motivation', value: 80 },
            { key: 'Vocals', value: 84 },
            { key: 'Sauce', value: 99 },
        ]
    },
]
