import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import { Container, Row, Col } from 'reactstrap'
import ReactLoading from 'react-loading'
import posed from 'react-pose';

import { BannerContainer, CenterHorizontally, colours, CenterVertically } from '../../styles'
import { animations, FadeDiv } from '../../animations'


const ScreenContainer = posed.div({
    cover: { opacity: 1, transition: { duration: 200 }},
    reveal: { opacity: 0, transition: { duration: 200 } }
})


const ScreenCover = (props) => {

    const { cover } = props;
    const pose = (cover === true) ? 'cover' : 'reveal';


    var classes = "screenCover "
    if (cover === false) {
        classes += ' hidden'
    }   

    // For now to test not using them
    classes = 'screenCover hidden';

    return (
        /* DISPLAY SET TO NONE ON App.css */
        <ScreenContainer pose={pose} className={classes}>
            <div className="background-colour fillParent">
                <CenterVertically>
                    <CenterHorizontally>
                        <ReactLoading height={30} width={30} type="spin" color={colours.primary} />
                    </CenterHorizontally>
                </CenterVertically>
            </div>
        </ScreenContainer>
    )
}


export default ScreenCover;
