import React, {Component} from 'react';
import { connect } from 'react-redux'
import {Route, Link, Switch, BrowserRouter as Router} from 'react-router-dom'
import {TransitionGroup, CSSTransition} from "react-transition-group";
import LazyLoad from 'react-lazyload'
//import ScreenCover from './ScreenCover';


// Routes --------
import Home         from    '../container/Home'
import Residents    from    '../container/Residents'
import Content      from    '../container/Content'
import ItemPage     from    '../container/Content/ItemPage'
import Hotline      from    '../container/Hotline'
import NoMatch      from    '../container/NoMatch'

// ---------------

export const pages = [
    {
        path: '/',
        title: 'Home',
        exact: true,
        component: () => <LazyLoad><Home/></LazyLoad>,
        inNav: true
    },
    {
        path: '/residents',
        title: 'Residents',
        component: () => <LazyLoad><Residents/></LazyLoad>,
        inNav: true
    },
    {
        path: '/content',
        title: 'Content',
        component: () => <LazyLoad><Content/></LazyLoad>,
        exact: true,
        inNav: true
    },

    {
        path: '/content/:id',
        title: 'ContentItem',
        component: (props) => <LazyLoad><ItemPage {...props} /></LazyLoad>,
        exact: true,
        inNav: false
    },

    {
        path: '/hotline',
        title: 'Hotline',
        component: () => <LazyLoad><Hotline/></LazyLoad>,
        inNav: true
    },
];



class NavigationContainer extends Component {


    state = {
        prevRoute: {},
        loading: true
    }

    componentWillReceiveProps(nextProps) {

        this.setState({ loading: (nextProps.loadItems > 0) ? true : false });

        if (nextProps.location.pathname != this.props.location.pathname) {
            this.setState({ 
                prevRoute: this.props.location,
             })
        }
    }

    render() {

        const { location } = this.props;

        return (
            <div className="body-container">
        
                <div className="navigation-container">
                    <Switch location={location}>
                        {
                            pages.map((page) => {
                                return <Route key={page.path, page.title} path={page.path} exact={page.exact} render={page.component}/>
                            })
                        }
                        
                        <Route component={NoMatch}/>
                    </Switch>
                </div>

            </div>
        )
    }
}



const mapStateToProps = state => {
    return { loadItems: state.loadItems };
  };

export default connect(mapStateToProps)(NavigationContainer);