
import React, { Component } from 'react';
import {Route, Link, Switch, BrowserRouter as Router} from 'react-router-dom'
import { Provider } from "react-redux";
import store from "./redux";
import ReactGA from 'react-ga'

import NavigationContainer    from './navigation'
import Header                 from  './component/Header'

import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import './styles/css/index.css'

ReactGA.initialize('UA-129120305-1');

class App extends Component {
  render() {
    return (
      <div className="App"> 

        <Provider store={store}>

          <Router>
            <Route render={({location}) => (
              <div className="app-body">

                <Header location={location}/>
                <NavigationContainer location={location}/>
                
              </div>
            )}/>
          </Router>

        </Provider>

      </div>
    );
  }
}

export default App;
