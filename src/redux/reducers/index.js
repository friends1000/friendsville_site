

  const ADD_LOAD_ITEM       = 'redux_addloaditem';
  const REMOVE_LOAD_ITEM    = 'redux_removeloaditem';
  const REFRESH_LOAD_ITEM   = 'redux_refreshloaditem';


  const initialState = {
    loadItems: 0
  };


  export default function rootReducer(state = initialState, action) {

    switch(action.type) {

      case ADD_LOAD_ITEM: {
        return { ...state, loadItems: state.loadItems + 1 }
      }
      
      case REMOVE_LOAD_ITEM: {
        return { ...state, loadItems: state.loadItems - 1 }
      }

      case REFRESH_LOAD_ITEM : {
        return { ...state, loadItems: 0 }
      }
    }

    return state;
  };
  




  export function addLoadItem() {
    return { type: ADD_LOAD_ITEM };
  }

  export function refreshLoadItems() {
    return { type: REFRESH_LOAD_ITEM };
  }

  export function removeLoadItem() {
    return { type: REMOVE_LOAD_ITEM };    
  }