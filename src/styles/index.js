import React from 'react';
import styled /*, { injectGlobal }*/ from 'styled-components';
import { injectGlobal } from 'styled-components';


const header_height         =   '60px';
const footer_height         =   '50px';
const inner_box_margins     =   '10px';
const page_padding          =   '20px';


export const colours = {
  /*
    background: 'white',
    colorA: '#EAF6FF', 
    colorB: '#D0CDD7',
    primary: '#43CDFF',
    secondary: '#FFB30F'
  */

    background: '#ACFCD9',
    colorB: '#F3FAF0', 
    colorA: '#C6DEA6',
    primary: '#29D8B5',
    secondary: '#E29D85'

}

//https://coolors.co/29d8b5-acfcd9-f3faf0-c6dea6-e29d85
//https://coolors.co/587291-2f97c1-1ccad8-15e6cd-0cf574





// ----------------------------------


export const AbsoluteCenter = (props) => {
  return (
    <CenterVertically className={"position-absolute " + props.className}>
      <CenterHorizontally>
        {props.children}
      </CenterHorizontally>
    </CenterVertically>
  )
}

// ------------

export const LockContainerCont = styled.div`
  position: absolute;
  background: rgba(0, 0, 0, 0.9);
  margin: -20px;
  z-index: 890;
  width: calc(100% + 40px);
  height: 500px;
  border-radius: 10px;
  overflow: hidden;

  h1 {
    padding-top: 30px;
  }
  p {
    color: white;
    font-size: 20px;
  }

  img {
    width: 80px;
    height: 80px;
    margin-bottom: 20px;
  }
`

// -------

export const TitleContainer = styled.div` 
  display: flex;
  flex-direction: row;
  
  h1, h2, h3, h4, h5, h6 {
    margin-right: ${page_padding};
  }

  ${props => (props.bottom) ? `margin-bottom: ${page_padding};` : null};
`


export const ContentBody = styled.div`

  margin-top: ${props => (props.fullTop === true) ? `calc(${page_padding} * 3)` : page_padding};
  margin-top: ${props => (props.noTop === true) ? '0px' : page_padding};
  margin-bottom: ${props => (props.noBottom === true) ? '0px' : page_padding};
  border-radius: ${props => (props.rounded === true) ? '20px' : null };

  padding: ${props => (props.largePadding) ? `calc(${page_padding} * 3)` : page_padding};
  padding: ${props => (props.noPadding) ? '0px' : page_padding};

  ${props => (props.noSideMargin) ? `margin-left: 0px; margin-right: 0px;` : null};



  ${props => (props.textLeft === true) ? 'text-align: left;' : null };

  ${props => (props.roundedTop === true) ? 
    `
    border-top-left-radius: 20px;
    border-top-right-radius: 20px;
    `
  : null }

  ${props => (props.roundedBottom === true) ? 
    `
    border-bottom-left-radius: 20px;
    border-bottom-right-radius: 20px;
    `
  :  null  }

  ${props => (props.shadow === true) ?
    `
    -webkit-box-shadow: 3px 8px 30px -7px rgba(0,0,0,0.6);
      -moz-box-shadow: 3px 8px 30px -7px rgba(0,0,0,0.6);
      box-shadow: 3px 8px 30px -7px rgba(0,0,0,0.6);
    `
  : null }

  background-color: ${props => props.background};


  .linkContainer {
    min-width: 50%;
  }

  .linkIcon {
    cursor: pointer;
    margin-top: 5px;
    margin-bottom: 5px;
    transition: all 0.3s;
  }

  .linkIcon:hover {
    transform: scale(1.4);



    transition: all 0.3s;
  }

  .word-break {
    white-space: pre-line;
  }
`

export const ContactFormContainer = styled.div`
  border-radius: 20px;
  background-color: ${colours.colorB};
  padding: ${page_padding};

  label {
    margin-bottom: 2px;
    color: gray;
    transition: all 0.2s;
  }

  .bold {
    font-weight: bold;
    transition: all 0.2s;
  }

  p {
    text-align: center;
    padding-top: ${page_padding};
  }

  p.success {
    color: ${colours.primary};
  }

  p.fail {
    color: ${colours.secondary};
  }
 
  input, textarea {
    width: 100%;
    margin-top: 0px;
    margin-bottom: 20px;   

    border: 0px; 
    border-radius: 2px;
    /*border-width: 0.2px;*/
    color: gray;
    padding: 10px;

    -webkit-box-shadow: none;
    -moz-box-shadow: none;
    box-shadow: none;

    outline: none;
    transition: all 0.4s;
  }

  input:focus, textarea:focus {
    transform: scale(1.08);
    border-radius: 5px;
    transition: all 0.4s;
  }

  textarea {
    max-height: 200px;
  }

  .buttonContainer, form .submit {

    input {
      margin: 0px;
      padding: 0px;
      background-color: transparent;
      color: white;
      text-transform: uppercase;
      font-weight: bold;
    }

    background-color: ${colours.primary};
    color: white;

    display: block;

    text-transform: uppercase;
    font-weight: bold;
    text-align: center;

    padding: 10px;
    border-radius: 10px;
    display: flex;
    flex-direction: row;
    justify-content: center;

    a {
      width: 100%;
      backgrond-color: red;
      text-align: center;
    }
    transition: all 0.4s;
   
  }

  .buttonContainer:hover, form .submit:hover {
    transform: scale(1.05);
    transition: all 0.4s;
  }
`

export const HeaderContainer = styled.div`
    height: ${header_height};
    /*background-color: ${colours.colorB};*/
    position: fixed;

    left: 0px;
    top: 0px;
    width: 100vw;
    z-index: 999;
    font-weight: bold;

    background-color: ${props => (props.background === true) ? colours.colorB : null };

    transition: all 0.4s;

    /*opacity: 0.3;*/

    img, .topIcon {
      position: relative;
      top: -6px;
      height: 30px;
      width: 30px;
    }

    a {
      color: ${colours.primary};
      float: right;
      transition: all 0.2s;
    }

    a:hover {
      transform: scale(1.2);
      text-decoration: none;
      transition: all 0.2s;
    }

    font-family: 'Avenir';

    .full-height {
      height: 100%;
    }

    p {
      padding: 0px;
      margin: 0px;
    }

    .loadingbar {
      background-color: ${colours.primary};
      border-radius: 5px;

      height: 5px;
      z-index: 890;
      top: ${header_height};
      left: 0px;
      width: 16%;
      position: absolute;
      transition: all 0.5s;
    }

    .loadingbar.hidden {
      display: none;
    }

    .non-responsive {
      display: none;
    }

    /* Medium Devices, Desktops */
    @media only screen and (min-width : 992px) {

      .responsive {
        display: none;
      }

      .non-responsive {
        display: block;
      }
    }

    .responsive {

      .menu {

        padding-top: ${page_padding};
        padding-bottom: ${page_padding};
        
        background-color: ${colours.colorB};
        z-index: 800;

        a, p {
          margin-top: 20px;
          margin-bottom: 20px;
        }

      }

      .menu.closed {
        pointer-events: none;
        display: none;
        transition-delay: all 1;
      }   

    }

    .responsive-body {

      display: flex;
      flex-direction: row;
      justify-content: space-between;

      .menuIcon {

        position: relative;
        top: 10px;

        background-color: white;
        padding: 10px;
        border-style: solid;
        border-color: ${colours.primary};
   
        border-radius: 50%;
        z-index: 999;

        transition: all 0.4s;       
      }

      .menuIcon.selected {
        transform: scale(1.2) rotate(360deg);
        transition: all 0.4s;        
      }   

    }
    

`


export const Page = styled.div`
    /*margin-top: ${header_height};*/
    /*padding-top: 20px;*/

    min-height: calc(100vh - ${footer_height} /* -${header_height} */);
    /*background-color: #ffb6c1;*/
    background-color: ${colours.background};

    padding-bottom: ${props => (props.noBottom) ? null : page_padding};
    padding-top: ${props => (props.top != undefined) ? `calc(${header_height} + ${page_padding});` : '10px' };

    p {
      font-family: 'Poppins', sans-serif;
      color: gray;
    }

    h1 {
      font-family: 'Fontanero Bevel';
      
      margin-top: 20px;
      margin-bottom: 40px;
      color: ${colours.primary};
      /*color: white;*/
      /*text-transform: uppercase;*/
    }

    .upArrow {
      background-color: rgba(0,0,0,0.6);
      border-radius: 100%;
      transition: all 0.1s;
      padding: 0px;
    }

    .upArrow:hover {
      top: 100px;
      padding: 4px;
      transition: all 0.1s;
    }


    .titleContainer {
      display: flex;
      flex-direction: row;

      
      h3 {
        color: ${colours.primary};
        text-transform: uppercase;
        font-size: 25px;
        padding-right: 10px;
        
      }

      .icon {
        background-color: red;
        padding: 10px;
        border-radius: 50%;
        position: relative;
        top: -10px;
        background-color: rgba(255,255,255,0.8);
      }
    }
    
  
    .videoContainer {
        height: 50vw;
        max-height: 400px;
        padding: 10px;
        background-color: ${colours.colorA};
        border-top-left-radius: 10px;
        border-top-right-radius: 10px;
        border-bottom-left-radius: 10px;
        border-bottom-right-radius: 10px;

      }

  .contentContainer {

    background-color: ${colours.colorA};
    border-radius: 20px;

   
      display: flex;
      flex-direction: row;
      justify-content: flex-start;
      flex-wrap: wrap;  
    

  }



  .loadContainer_container {

    .absolute_fill {
      position: absolute;
      left: 0;
      top: 0;
      height: 100%;
      width: 100%;
    }

    .hidden {
      opacity: 0;
      transition: all 0.3;
    }
  }


`

export const LazyContainer = styled.div`


background-color: red;

  .lazy {
    opacity: 1;
  } 

  .lazy.hidden {
      display: none;
  }

  .main.visible {
    opacity: 1;
  }

  .main.hidden {
      position: absolute;
      opacity: 0;
  }

    

`

export const ContentImage = styled.div`

  ${props => (props.top) ? `margin-top: ${page_padding};` : null };
  ${props => (props.rounded) ? `border-radius: 10px;` : null };

  width: 100%;

  img {
    width: 100%;
  }

`

export const LoadScreen = styled.div`
  position: absolute;
  left: 0;
  top: 0;
  bottom: 0;
  right: 0;
  width: 100vw;
  height: 100vh;
  background-color: ${colours.background};

  z-index: 830;
`


export const SelectionBox = styled.div`

    -webkit-box-shadow: 3px 8px 30px -7px rgba(0,0,0,0.6);
    -moz-box-shadow: 3px 8px 30px -7px rgba(0,0,0,0.6);
    box-shadow: 3px 8px 30px -7px rgba(0,0,0,0.6);

    margin-top: 18px;
    margin-bottom: 18px;
    max-height: 50vw;
    border-radius: 10px;
    overflow: hidden;
   
    cursor: pointer;

    p {
      color: white;
    }

    .sectinBody {
      border-radius: 2px;
      background-color: ${colours.colorA};
      
      margin-top: 10px;
      margin-bottom: 10px;

      padding-left: 0px;
      padding-right: 0px;
      /*padding-bottom: calc(${inner_box_margins} - 5px);*/
    }

    .sectinBody:hover .details {
      background-color: red;
    }

    .imageContainer {
        width: 100%;
    }

    .image {
        object-fit: cover;
        width: 100%;
    }

    .details {
        background-color: rgba(0, 0, 0, 0.5);
        color: white;

        position: absolute;
        width: 100%;
        padding: 10px;

        border-bottom-left-radius: 10px;
        border-bottom-right-radius: 10px;
        
        bottom: 0px;
        left: 0px;
        margin-top: ${inner_box_margins};

        min-height: 60px;
        transition: all 0.2s
    }

    /*
    .details:hover {
      background-color: rgba(0, 0, 0, 0.8);
      padding-bottom: 180px;
      transition: all 0.2s;
    }
    */

    /* could probably animate description in tbh */
`

export const VillaSlotContainer = styled.div`
  
    background-color: ${colours.colorB};
    height: 20px;
    width: 20px;

    display: flex;
    flex-direction: row;

    width: 100%;
    height: 200px;

    margin-top: 20px;
    margin-bottom: 20px;

    .image {
      /*height: 100%;*/
      height: 200px;
      width: 200px;
      overflow-y: hidden;
      overflow-x: hidden;

      img {
        height: 100%;
      }
    }

    .details {
      flex: 1;
      height: 100%;
      padding-top: 10px;
      padding-bottom: 10px;
      max-width: 150px;
    }

    .pricing {
      flex: 1;
      height: 100%;
      padding-top: 10px;
      padding-bottom: 10px;
    }

`


export const ResidentContainer = styled.div`

    width: 100%;
    height: 100%;

    display: flex;
    flex-direction: row;
    justify-content: center;

    margin-bottom: ${page_padding};

    .info {
      z-index: 700;
      background-color: white;

      border-radius: 50%;
      z-index: 700;
      cursor: pointer;

      transition: transform 0.3s
    }

    .details-container {
      position: absolute;

      left: 20%;
      top: 10%;
      width: 300px;
      

      .statContainer {

        .stat {

          margin-top: 2px;
          margin-bottom: 2px;
          height: 20px;

          display: flex;
          flex-direction: row;
          justify-content: flex-start;

          .key {
            width: 150px;
          }

          .value {
            width: 80px;
          }

          .bar-container {
            width: 130px;
          }
          
          .statbar {
            position: relative;
            top: 5px;
            background-color: ${colours.primary};
            height: 15px;
            width: 100%;
          }

        }

      }
    }

    .info:hover {
      transform: scale(1.2) rotate(30deg);
      transition: transform 0.3s
    }

    img {
      max-height: 50vh;      
    }

`


export const FooterContainer = styled.div`

  background-color: ${colours.colorB};
  height: ${footer_height};
  width: 100%;

  .buttonContainer {
      width: 100%;
      display: flex;
      flex-direction: row;
      justify-content: space-between;

      div {

        position: relative;
        scale: 1;
        transition: all 0.3s;
        padding: 10px;

      }

      div:hover {

        transform: scale(1.5);
        border-radius: 100%;
        background-color: ${colours.colorB};
        transition: all 0.3s;
        
      }

    }
`

export const BannerContainer = styled.div`
    width: 100%;
    /*height: 400px;*/
    margin-bottom: ${page_padding};
    overflow-y: hidden;
    overflow-x: hidden;

    img {
      width: 100%;
    }
`



export const ContentHolder = styled.div`
    background-color: ${props => props.background};

    margin-top: ${props => (props.top != undefined) ? page_padding : null };
    margin-bottom: ${props => (props.bottom != undefined) ? page_padding : null };

    padding-top: calc(${page_padding} * 2);
    padding-bottom: calc(${page_padding} * 2);

    border-radius: ${props => (props.rounded === true) ? '20px' : null };

    /*min-height: 200px;*/
    width: 100%;

    .mc-field-group {
      margin-top: 20px;
      display: flex;
      flex-direction: column;
    }

`


export const CenterVertically = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  height: 100%;
`

export const CenterHorizontally = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
  width: 100%;
`

export const TopDip = styled.div`
    background-color: ${props => props.background};
    height: ${props => (props.height) ? props.height + 'px' : '60px'};

    MARGIN-top: ${props => (props.top) ? '20px' : null };
    top: ${page_padding};

    width: 100%;
    position: relative;
    

    ${props => {
      
      if (props.left) {
        return (`
          -webkit-clip-path: polygon(0% 0, 100% 80%, 100% 100%, 0 100%);
          clip-path: polygon(0% 0, 100% 80%, 100% 100%, 0 100%);
          `
        )
      }

      if (props.right) {
        return (`
          -webkit-clip-path: polygon(100% 0, 100% 100%, 100% 100%, 0 100%);
          clip-path: polygon(100% 0, 100% 100%, 100% 100%, 0 100%);
          `
        )
      }

      if (props.center) {
        return `
          border-top-left-radius: 100% 100px;
          border-top-right-radius: 100% 100px;
        `
      }
    }}
`

export const BottomDip = styled.div`
    background-color: ${props => props.background};
    height: 60px;
    top: -${page_padding};

    width: 100%;
    position: relative;
    overflow: hidden;

    ${props => {
      
      if (props.left) {
        return (`
        -webkit-clip-path: polygon(0% 0, 100% 80%, 100% 100%, 0 100%);
          clip-path: polygon(0% 0, 100% 80%, 100% 100%, 0 100%);
          `
        )
      }

      if (props.right) {
        return (`
        -webkit-clip-path: polygon(100% 0, 100% 100%, 100% 100%, 0 100%);
          clip-path: polygon(100% 0, 100% 100%, 100% 100%, 0 100%);

          `
        )
      }

      if (props.center) {
        return `
          border-bottom-left-radius: 100% 100px;
          border-bottom-right-radius: 100% 100px;
        `
      }
    }}
`












//😂😂😂😂😂😂😂😂😂😂😂
//ctrl + cmd + space

