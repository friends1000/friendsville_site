import React, { Component } from 'react';
import posed from 'react-pose';
import { connect } from 'react-redux';


// Defaults - can all easily be overwritten
const defaultDelay  = 200;
const fadeMotion    = 30;




export const animations = {
    fadeIn: {
        start: { opacity: 0, delay: defaultDelay },
        end: { opacity: 1, delayChildren: 1000 }
    },

    fadeOut: {
        start: { opacity: 1, delay: defaultDelay },
        end: { opacity: 0, delayChildren: 1000 }
    },

    fadeLeft: {
        start: { opacity: 0, x: fadeMotion, delay: defaultDelay },
        end: { opacity: 1, x: 0 }
    },

    fadeRight: {
        start: { opacity: 0, x: -fadeMotion, delay: defaultDelay },
        end: { opacity: 1, x: 0 }
    },

    fadeUp: {
        start: { opacity: 0, y: fadeMotion, delay: defaultDelay },
        end: { opacity: 1, y: 0,  }
    },

    fadeDown: {
        start: { opacity: 0, y: -fadeMotion, delay: defaultDelay },
        end: { opacity: 1, y: 0,  }
    },
    
    children: {
        fadeRight: {
            end: { opacity: 1, x: 0 },
            start: { opacity: 0, x: -fadeMotion }
        },

        fadeLeft: {
            end: { opacity: 1, x: 0 },
            start: { opacity: 0, x: fadeMotion }
        },

        fadeUp: {
            end: { opacity: 1, y: 0 },
            start: { opacity: 0, y: fadeMotion }
        },

        fadeDown: {
            end: { opacity: 1, y: 0 },
            start: { opacity: 0, y: -fadeMotion }
        }
    }
}




class FadeDivCont extends Component {
    AnimDiv = undefined;

    
    /*
    componentWillReceiveProps(nextProps) {

        const { props } = this;

        // When loading is finished for page - then animate
        if (nextProps.loadItems === 0 && props.delay != undefined) {
            setTimeout(() => { this.setState({ pose: 'end' }) }, props.delay);
        }

    }*/



    constructor(props) {
        super(props)
        this.state = { pose: 'start' }
        
        var animation = { ...props.animation };
        
        if (props.childDelay)   animation.end.delayChildren     =   props.childDelay;
        if (props.childStagger) animation.end.staggerChildren   =   props.childStagger;
        if (props.delay)        animation.start.delay           =   props.delay;

        if (props.debug) console.log(animation)

        //animation.end.transition = {duration: 1000 };
        
        this.AnimDiv = posed.div(animation)

        if (props.delay != undefined) {
            setTimeout(() => { this.setState({ pose: 'end' }) }, props.delay);
        }

        /*
        // Double check to see if there is anything to load - means pages with load content still work fines
        setTimeout(() => {
            // Check if there is nothing to load - then carry on and animate
            if (this.props.loadItems === 0 && this.props.delay != undefined) {
                setTimeout(() => { this.setState({ pose: 'end' })}
                , this.props.delay );
            }
        }, 100)
        */

    }

    render() {

        const { AnimDiv, props, state } = this;
        const { children, debug } = props;
        const { pose } = state;

        const updateState = this.props.state;   // If the user wants to manually set state

        if (debug === true) console.log(pose)
        
        return (
            <AnimDiv pose={updateState || pose}>
                {children}
            </AnimDiv>
        )

    }
}


const mapStateToProps = state => {
    return { loadItems: state.loadItems };
};
  

export const FadeDiv = connect(mapStateToProps)(FadeDivCont)

FadeDiv.displayName = 'FadeDiv'


