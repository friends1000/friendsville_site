
import React, { Component } from 'react';

import { Link } from 'react-router-dom'
import { Container, Row, Col } from 'reactstrap'
import ReactLoading from 'react-loading';
import Helmet from 'react-helmet'

/*
import MapIcon    from  'react-icons/lib/fa/map-marker'
import GlobeIcon  from  'react-icons/lib/fa/globe'
import ArrowIcon  from  'react-icons/lib/fa/arrow-right'
import ArrowUp    from  'react-icons/lib/fa/angle-up'
import HouseIcon  from  'react-icons/lib/fa/home'
*/

import { SEO } from '../../data/SEO'
import ReactGA from 'react-ga'


import { FadeDiv, animations } from '../../animations' 
import { 
  Page, 
  ContentHolder, 
  TopDip, 
  BottomDip, 
  colours, 
  CenterVertically, 
  CenterHorizontally,
  AbsoluteCenter,
  ContentBody
} from '../../styles'


import Footer       from    '../../component/Footer'
import ContactForm  from    '../../component/ContactForm'


const titleDelay= 100;
const baseDelay = 200;
const itemDelay = 200;
const itemStagger = 100;




export default class Hotline extends Component {

  constructor(props) {
    super(props);

    ReactGA.pageview('/hotline');
  }


  render() {

      const seoData = SEO.hotline;
   
      return (

          <div>

              <Helmet title={seoData.title} metaname={seoData.metaname} description={seoData.description}  />

              <Page top>

                  <FadeDiv animation={animations.fadeUp} delay={titleDelay}>
                      <CenterHorizontally className="text-center">
                          <h1>Hotline</h1>
                      </CenterHorizontally>
                  </FadeDiv>

                    <CenterVertically>
                    <ContentHolder top background={colours.colorA}>

                    <Container>
                    {/*
                    <h3>The F1K Hotline</h3>
                    <h5>Direct 2 way communication</h5>
                    */}

                    <FadeDiv animation={animations.fadeUp} delay={baseDelay} childStagger={itemStagger} childDelay={itemDelay}>

                    <Col xs={12} sm={{size: 8, offset: 2}} md={{size:6, offset: 3}} lg={{size: 4, offset: 4}}>
                        <ContactForm />
                    </Col>

                    </FadeDiv>

                    </Container>
                   </ContentHolder>
                   </CenterVertically>

     
 
              </Page>

              <Footer />
          </div>
      )

  }
}




