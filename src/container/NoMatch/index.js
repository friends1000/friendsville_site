
import React, { Component } from 'react';
import { Redirect } from 'react-router-dom'
import { Col, Container, Row } from 'reactstrap'
import { FadeDiv, animations } from '../../animations' 
import { 
  Page, 
  CenterHorizontally,
  ContentBody
} from '../../styles'

import Footer           from    '../../component/Footer'
import ContentBox       from    '../../component/ContentBox'
import { getContentPageContent } from '../../data'

const titleDelay= 100;
const itemDelay = 100;
const itemStagger = 100;

const latestContent = getContentPageContent().splice(0, 3);


console.log(latestContent)

export default class NoMatch extends Component {

  state = { }

  _itemClick = (id) => {
    this.setState({ redirect: '/content/' + id })
  }

  render() {

      const { redirect } = this.state;
      const { _itemClick } = this;

      if (redirect) return <Redirect to={redirect}/>
   
      return (

          <div>
              <Page top>

                  <FadeDiv animation={animations.fadeUp} delay={titleDelay} childDelay={itemDelay} childStagger={itemStagger}>
                      <CenterHorizontally className="text-center">
                          <h1>NO MATCH</h1>                          
                      </CenterHorizontally>

                      <FadeDiv animation={animations.fadeUp}>
                        <div className="text-center">
                          <h5>You seemed to have got a dead link; if you think there should be something here, contact us.</h5>
                        </div>
                      </FadeDiv>

                      <Container>

                      <ContentBody className="text-center">
                        <p>Alternativly, check out our latest content</p>

                        <ContentBody>
                        <Row>
                        
                          {
                            latestContent.map((item) => {
                              return <Col sm={12} md={6} lg={4}>
                                <ContentBox data={item} key={item.id} onClick={_itemClick}/>
                              </Col>
                            })
                          }
               
                        </Row>
                        </ContentBody>

                        
                      </ContentBody>
                      </Container>
                  </FadeDiv>
 
              </Page>

              <Footer />
          </div>
      )

  }
}




