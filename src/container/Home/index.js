
import React, { Component } from 'react';
import Helmet from 'react-helmet'
import { Link, Redirect } from 'react-router-dom'
import { Container, Row, Col } from 'reactstrap'

import MapIcon    from  'react-icons/lib/fa/map-marker'
import GlobeIcon  from  'react-icons/lib/fa/globe'

import { getContentPageContent } from '../../data/index';
import { SEO } from '../../data/SEO';
import ReactGA from 'react-ga';

import { FadeDiv, animations } from '../../animations' 
import { 
  ContentHolder, 
  colours, 
  CenterVertically, 
  CenterHorizontally,
  TitleContainer,
  ContentBody,
  ContactFormContainer,
  ContentImage
} from '../../styles'
import Page             from    '../../component/Page'
import ContentBox       from    '../../component/ContentBox'
import LoadContainer    from    '../../component/LoadContainer' 
import LazyImage        from    '../../component/LazyImage'
import {LazyLoadImage}  from    'react-lazy-load-image-component'
import Footer           from    '../../component/Footer'



const gangA = require('../../asset/img/press/gangA.jpg');
const gangAComp = require('../../asset/img/press/gangA_comp.jpg')

const gangB = require('../../asset/img/press/gangB.jpg');
const gangBComp = require('../../asset/img/press/gangB_comp.jpg');

const titleDelay = 20;
const contentDelay = 100;
const contentStagger = 200;

const featuredContent = getContentPageContent().slice(0,3);


export default class Home extends Component {

  constructor(props) {
    super(props);

    this.state = { isLoading: true }
    ReactGA.pageview('/');
    
  }


  componentDidMount() {
     //this.setState({isLoading: false})
     setTimeout(() => {
       this.setState({ isLoading: false })
     }, 600);
  }


  _onItemClick = (id) => {
    ReactGA.event({
        category: 'Selected ', //from Home',
        action: 'ID: ' + id
    });
    this.setState({ redirect: '/content/' + id })
  }

  
  render() {

    const { isLoading, redirect } = this.state;
    const { pageData, _onItemClick } = this;

    if (redirect != undefined) {
        return (<Redirect to={redirect}/>)
    }

    const seoData = SEO.home

    return (

        <div>

        <Helmet title={seoData.title} metaname={seoData.metaname} description={seoData.description}  />


      <Page top noBottom > 
        
        <FadeDiv animation={animations.fadeUp} delay={titleDelay}>
            <CenterHorizontally className="text-center">
                <h1>Welcome to Friendsville</h1>
            </CenterHorizontally>
        </FadeDiv>


        <Container>      
            <div className="videoContainer">
                <LoadContainer noLoadIcon>
                    <iframe width="100%" height="100%" src="https://www.youtube.com/embed/5OGne8UvLnU" frameBorder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowFullScreen></iframe>
                </LoadContainer>
            </div>  
        </Container>


        <ContentBody fullTop>
            <Container>
                <div className="text-center subtitlePadding">
                    <h3>Latest Content</h3>
                </div>

                <div>
                    <Row>
                        { featuredContent.map((item, index) => {
                            return (
                                <Col xs={12} sm={6} lg={4} key={'home_latest-' + index}>
                                    <ContentBox data={item} delay={contentDelay + contentStagger * index} onClick={_onItemClick} />
                                </Col>
                            )
                        })}
                    </Row>
                </div>
            </Container>
        </ContentBody>

        <ContentHolder background={colours.background}>
            <Container>
                <ContentBody noTop background={colours.colorB} rounded>
                <Row>
                    <Col sm={{size: 12}} md={{size: 6}}>
                        <ContentBody>

                            <TitleContainer>
                                <h3>What is Friendsville?</h3>
                                <MapIcon size={30} color={colours.primary}/>
                            </TitleContainer>
                        
                        <p>
                        Friendsville is Friends1000's caricature of life. The world through our eyes. 
                        </p>
                        <p>
                        Loosly based on Manchester (all born and bred), Friendsville is a metropolitan area 
                        located near The Village Hidden in the Sauce. Recently growing in both economic power and popularity, 
                        Friendsville is a must-see destination for business and leisure alike.
                        </p>
                        </ContentBody>
                    </Col>

                    
                    <Col sm={12} md={6}>
                        <ContentImage rounded>
                            <LoadContainer noLoadIcon>
                                <LazyLoadImage lazy={gangAComp} src={gangA}  alt={'Gang A'}/>
                            </LoadContainer>
                        </ContentImage>
                    </Col>
                    
                    <Col sm={12} md={6}>
                        <ContentImage top rounded>
                            <LoadContainer  noLoadIcon>
                                <LazyLoadImage lazy={gangBComp} src={gangB} alt={'Gang B'}/>
                            </LoadContainer>
                        </ContentImage>
                    </Col>


                    {/* POSSIBLE HAVE IMAGE HERE  */}
                    <Col sm={{size: 12, offset: 0}} md={{size: 6, offset: 0}}>
                        
                        <ContentBody>
                        <TitleContainer>
                            <h3>What we do</h3>
                            <GlobeIcon size={30} color={colours.primary}/>
                        </TitleContainer>

                        <p>
                        In short - whatever we want. Primarily music.
                        </p>
                        
                        <p>
                        We're a creative group who've known eachother for years (4/5 attended same high school). 
                        Having a passion for most things creative and fun, music is our first venture into the industry. 
                        </p>

                        <p>
                            Our goal is to have the best life possible doing the things we enjoy with gang. 
                            What's better than capitalism with friends?
                        </p>
                        </ContentBody>
                    </Col>
                </Row>
                </ContentBody>
            </Container>
        </ContentHolder>


        <ContentHolder background={colours.colorA} top>
            <Container>

            <Row>
                <Col xs={{offset: 1, size: 10}} md={{size: 6, offset: 0}} sm={{offset: 0, size: 12}} >
                    
                    <TitleContainer bottom>
                        <h2>Sign up for updates</h2>                     
                    </TitleContainer>

                    <p>Social media algorithms are inefficient, sometimes they show posts - sometimes they don't.</p>
                    <p>
                        We added this feature because a lot of people were missing new content when it came out.
                        The email list is strictly for new content and big announcements (IE performing in your area) 
                        you don't have to worry about random emails. If you want to opt out, you can email back at any time saying so.
                    </p>
                </Col>

                <Col sm={12} md={6}>
                
                <CenterVertically>
                <ContactFormContainer   >

                <div id="mc_embed_signup">
                    <form action="https://friendsville.us7.list-manage.com/subscribe/post?u=c36950aede5d8c732a2866720&amp;id=b3eaebe57f" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" className="validate" target="_blank" noValidate>
                        <div id="mc_embed_signup_scroll">
                        
                        <Col>
                        <div className="mc-field-group">
                            <label htmlFor="mce-EMAIL">Email Address  <span className="asterisk">*</span></label>
                            <input type="email"  name="EMAIL" className="required email" id="mce-EMAIL"/>
                        </div>
                        </Col>
                
                    {/*<div id="mce-responses" className="clear">
                            <div className="response" id="mce-error-response" style="display:none"></div>
                            <div className="response" id="mce-success-response" style="display:none"></div>
                        </div>    */}
                        
                        {/*<!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->*/}
                        <Col>
                        {/*<div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_c36950aede5d8c732a2866720_b3eaebe57f" tabindex="-1" value=""/></div>*/}
                        <div className="clear submit">
                            <input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" className="button"/>
                        </div>
                        </Col>
                        </div>
                    </form>
                    </div>
                    {/*<script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[1]='FNAME';ftypes[1]='text';fnames[2]='LNAME';ftypes[2]='text';fnames[3]='ADDRESS';ftypes[3]='address';fnames[4]='PHONE';ftypes[4]='phone';fnames[5]='BIRTHDAY';ftypes[5]='birthday';}(jQuery));var $mcj = jQuery.noConflict(true);</script>*/}

                </ContactFormContainer>
                </CenterVertically>
                </Col>
            </Row>

            </Container>
        </ContentHolder>
        
      </Page>

        <Footer/>
      </div>
    )

  }
}
