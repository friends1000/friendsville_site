
import React, { Component } from 'react';
import Helmet from 'react-helmet'
import { Container, Row, Col } from 'reactstrap'
import ReactGA from 'react-ga'


import MapIcon    from  'react-icons/lib/fa/map-marker'
import GlobeIcon  from  'react-icons/lib/fa/globe'


import { FadeDiv, animations } from '../../animations' 
import { 
  Page, 
  ContentHolder, 
  TopDip, 
  BottomDip, 
  colours, 
  CenterVertically, 
  CenterHorizontally,
  AbsoluteCenter,
  ContentBody
} from '../../styles'
import Resident     from    '../../component/Resident'
import Footer       from    '../../component/Footer'

import { residents } from '../../data'
import { SEO } from '../../data/SEO'

const titleDelay= 100;
const baseDelay = 200;
const itemDelay = 200;


// Anayltics ----



export default class Residents extends Component {

  constructor(props) {
    super(props);

    this.state = { isLoading: true }
    ReactGA.pageview('/residents');
  }

  componentDidMount() {
     //this.setState({isLoading: false})
     setTimeout(() => {
       this.setState({ isLoading: false })
     }, 600);
  }


  render() {

    //const { isLoading } = this.state;
    const seoData = SEO.residents;
    
      return (

          <div>

            <Helmet title={seoData.title} metaname={seoData.metaname} description={seoData.description}  />


              <Page top>

                  <FadeDiv animation={animations.fadeUp} delay={titleDelay}>
                      <CenterHorizontally className="text-center">
                          <h1>Residents</h1>
                      </CenterHorizontally>
                  </FadeDiv>

                  <ContentHolder top>
                      <Container>

                          <Row>
                              {
                                  residents.map((item, index) => {
                                      return (
                                          <Col key={'resident-' + item.name} xs={12} sm={{ size: 8, offset: 2 }} md={{ size: 6, offset: 0 }} lg={{ size: 4, offset: 0 }} >

                                              <Resident data={item} delay={baseDelay + itemDelay * index} />

                                          </Col>
                                      )
                                  })
                              }

                          </Row>
                      </Container>
                   </ContentHolder>

 
              </Page>

              <Footer />
          </div>
      )

  }
}




