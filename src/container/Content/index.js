import React, { Component } from 'react';
import Helmet from 'react-helmet'
import { Link, Redirect } from 'react-router-dom'
import { Container, Row, Col } from 'reactstrap'
import posed from 'react-pose'
import ReactGA from 'react-ga'

import { 
    ResidentContainer, 
    colours, 
    CenterHorizontally, 
    CenterVertically, 
    ContentBody,
    Page
} from '../../styles'
import { animations, FadeDiv } from '../../animations'

import ContentBox   from    '../../component/ContentBox'
import Footer       from    '../../component/Footer'

import { SEO } from '../../data/SEO'

import Info         from    'react-icons/lib/fa/info-circle'
import Close        from    'react-icons/lib/fa/close'

import { content, getContentPageContent }  from    '../../data/index'

const titleDelay    = 20;
const contentDelay  = 200;
const contentChildDelay     =   300;
const contentChildStagger   =   200;   

function openLink(link) {
    window.open(link,'_blank');
}


const DetailsBox = posed.div({
    open: { opacity: 1, x: 0, y: 0, scale: 1 },
    closed: { opacity: 0, x: -5, y: -5, scale: 0.2 }
})





export default class Content extends Component {

    state = {};

    constructor(props) {
        super(props);
        ReactGA.pageview('/content');
    }

    _selectItem = (id) => {
        ReactGA.event({
            category: 'Selected', //from Content',
            action: 'ID: ' + id
        });
        this.setState({ redirect: 'content/' + id })
    }


    render() {

        const { _selectItem } = this;
        const { redirect } = this.state;
        // Select the link, if there is one redirect

        if (redirect != undefined) {
            return (<Redirect to={redirect} />)
        }

        var contentArr = [];

        const seoData = SEO.content;
        const contentItems = getContentPageContent();

        contentItems.map((item, index) => {
            contentArr.push (
                <Col key={'-cotent-container_' + item.id} xs={12} sm={6} md={4} lg={3}>
                    <ContentBox key={'-content-' + item.id} data={item} onClick={_selectItem} delay={200 * index + contentDelay} />
                </Col>
            )
        })

        return (
            <div>
                <Helmet title={seoData.title} metaname={seoData.metaname} description={seoData.description}  />

                <Page top>
                    <Container>
                        <div className="text-center">
                        <FadeDiv animation={animations.fadeUp} delay={titleDelay}>
                            <h1>Content</h1>
                        </FadeDiv>
                        </div>

                        <ContentBody>

                            
                            <Row>
    
                                    <div className="contentContainer">
                    
                                        {   
                                            contentArr
                                        }
                                
                                    </div>                                
            
                           </Row>
                           

                        </ContentBody>


                    </Container>
                </Page>
                <Footer />
            </div>
        )
    }
    
}



