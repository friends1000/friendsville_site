import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom'
import Helmet from 'react-helmet'
import { Container, Row, Col } from 'reactstrap'
import posed from 'react-pose'
import { LazyLoadImage } from 'react-lazy-load-image-component';
import ReactGA from 'react-ga';

import { SEO } from '../../../data/SEO'

import Instagram    from    'react-icons/lib/fa/instagram'
import Twitter      from    'react-icons/lib/fa/twitter'
import Youtube      from    'react-icons/lib/fa/youtube-play'
import Soundcloud   from    'react-icons/lib/fa/soundcloud'
import Spotify      from    'react-icons/lib/fa/spotify'
import AppleMusic   from    'react-icons/lib/fa/apple'

import { 
    colours, 
    CenterHorizontally, 
    CenterVertically, 
    ContentBody,
    ContentImage,
    Page,
} from '../../../styles'
import { animations, FadeDiv } from '../../../animations'

import ContentContainer     from    '../../../component/ContentContainer'
import Footer               from    '../../../component/Footer'
import LockContainer        from    '../../../component/LockContainer'

import Info         from    'react-icons/lib/fa/info-circle'
import Close        from    'react-icons/lib/fa/close'

import { content, linkTypes, contentTypes }  from    '../../../data/index'
import LoadContainer from '../../../component/LoadContainer';
import LazyImage from '../../../component/LazyImage';


const linkIcons = {
    [linkTypes.youtube]: Youtube,
    [linkTypes.applemusic]: AppleMusic,
    [linkTypes.spotify]: Spotify,
    [linkTypes.soundcloud]: Soundcloud
}

const embedHeights = {
    [linkTypes.youtube]: 400,
    [linkTypes.soundcloud]: 166,
    [linkTypes.spotify]: 80,
    [linkTypes.applemusic]: 200
}

const iconSize      = 40
const iconColour    = colours.primary;
 

function openLink(link) {
    window.open(link,'_blank');
}

const DetailsBox = posed.div({
    open: { opacity: 1, x: 0, y: 0, scale: 1 },
    closed: { opacity: 0, x: -5, y: -5, scale: 0.2 }
})



export default class ItemPage extends Component {

    state = {}

    constructor(props) {
        super(props)

        const id = props.location.pathname.split('/content/')[1]
        this.state.id = id;
    }

    componentWillMount() {
        this.loadData();
    }

    componentWillReceiveProps(props) {
        const id = props.location.pathname.split('/content/')[1]
        this.setState({ id: id })
        this.loadData(id);
    }

    loadData = (func_id) => {
        const id = func_id || this.state.id;

        ReactGA.pageview('/content/' + id);
        console.log('Loading data for ID: ' + id);

        for (var i = 0; i < content.length; i ++) {
            const item = content[i];
            if (item.id === id) {
                this.setState({ data: item });
                break;
            }
        }
    }

    _selectItem = (id) => {
        const { data } = this.state;

        ReactGA.event({
            category: 'Selected ', //from: ' + data.id,
            action: 'ID: ' + id
        });
        this.props.history.push('/content/' + id);
    }

    _processLinks = (displayItems) => {

        const { data } = this.state;
        const { links, id, type } = data;

        if (links === undefined || links === null || links.length === 0) return null // If invalid - skip

        var itemArr = [];
        const keys = Object.keys(links)


        keys.map((key) => {

            const item = links[key]
            const { embed, display, link, release_date } = item;

            if (displayItems === true && display != true) return null;

            var content     =   null;
            const Icon      =   linkIcons[key]
            const isVideo   =   (key === linkTypes.youtube && displayItems === true) 
            const height    =   (type === contentTypes.project) ? embedHeights[key] * 2.7 : embedHeights[key];

            if (embed != undefined && display === true && displayItems === true) {
                content = <LoadContainer><iframe width="100%" height={height} scrolling="no" frameBorder="no" allow="autoplay" allowFullScreen src={item.embed}></iframe></LoadContainer>
            } else {
                content = <div className="linkIcon"><Icon size={iconSize} color={iconColour} onClick={() => { openLink(link) }}/></div>
            }

            // Link container class means it's min width is 50%, being 
            // If is a video - take up full width
            itemArr.push(
                <Col key={id + '_link:' + key} style={(displayItems === true) ? {minWidth: '50%'} : {minWidth: '10%'}} xs={(displayItems === true) ? 12 : 'auto'} sm={(isVideo === true) ? 12 : 6} md={(isVideo === true) ? 12 : true}>
                    { content }
                </Col>
            )
        })

        return itemArr;
    }

    _renderConnections = () => {

        const connectionArr = this._processConnections();
        if (connectionArr === null ) return null;


        return (
            <ContentBody background={colours.colorA} largePadding noBottom >
                <Container>
                    {connectionArr}        
                </Container>
            </ContentBody>
        )
       
    }

    _processConnections = () => {
        const { _selectItem } = this;
        const { data } = this.state;
        const { connections } = data;

        if (connections === undefined) return null

        const keys = Object.keys(connections);
        var connectionArr = [];

        for (var i = 0; i < keys.length; i ++) {
            const item = connections[keys[i]];

            // If doesnt have any inner items - skip
            if (item.items.length === 0) {
                continue;
            }
            
            connectionArr.push(
                <ContentContainer key={'contentContainer_' + keys[i]} title={keys[i]} data={item} onClick={_selectItem}/>
            )
        }

        return (connectionArr.length > 0) ? connectionArr : null;
    }

    render() {


        const { _selectItem, _processLinks, _processConnections, _renderConnections } = this;
        const { data } = this.state;

        var isLocked = false;

        if (data === undefined) {
            return (<Page top><CenterHorizontally><p>'MISSING CONTENT'</p></CenterHorizontally></Page>)
        }

        const { id, title, description, image, release_date, type, tag, connections, links } = data;
        const date = (release_date != undefined) ? release_date.toISOString().split('T')[0] : 'N/A';
        if (date != 'N/A') {
            if (new Date() < release_date) {
                isLocked = true;
                console.log(release_date)
            }
        }

        return (
            <div className="background-colour">

                <Helmet title={'F1K - ' + title} description={description} />

                <Page top noBottom>
                    <Container className="text-center">
                        <FadeDiv animation={animations.fadeUp} delay={20}>
                            <h1>{title}</h1>
                        </FadeDiv>
                        
                        <FadeDiv animation={animations.fadeUp} delay={200} childDelay={300} childStagger={200} >
                            
                            
                            <ContentBody background={colours.colorB} noTop noBottom rounded>

                                {(isLocked === true) ?
                                    <LockContainer date={release_date}/>
                                    :
                                    null
                                }

                                <Row>
                                    <Col xs={12} sm={8} md={6} lg={4} >
                                        <FadeDiv animation={animations.fadeRight}>
                                            <ContentImage>
                                                <LoadContainer>
                                                    <LazyLoadImage src={data.image} width={300} height={300}/>                                                    
                                                </LoadContainer>
                                            </ContentImage>
                                            <div className="">
                                                {  
                                                    (date != 'N/A') ?
                                                    <p>{date}</p>
                                                    : 
                                                    null
                                                }
                                            </div>
                                        </FadeDiv>
                                    </Col>

                                    <Col >

                                    <ContentBody textLeft noTop noBottom noPadding noSideMargin background={colours.colorA}>
                                    <Col sm={12} md={12}>

                                        <ContentBody background={colours.colorA} noBottom noSideMargin noTop>

                                            <CenterHorizontally>
                                                <h5>Links</h5>
                                            </CenterHorizontally>
                                            <Row className="text-center">
                                                {_processLinks(false)}
                                            </Row>

                                        </ContentBody>

                                    </Col>
  
                                    </ContentBody>
                                            
                                        
                                        <Col className="word-break">
                                            <FadeDiv animation={animations.fadeUp}>
                                                <ContentBody noTop>
                                                {description}
                                                </ContentBody>
                                            </FadeDiv>
                                        </Col>
                                        
                                    </Col>

                        
                                </Row>
                            </ContentBody>
                        </FadeDiv>

                        <ContentBody>
                            <Row>
                            {
                                (isLocked === false) ?
                                    _processLinks(true)
                                : null
                            }
                            </Row>
                        </ContentBody>   


                        <ContentBody>
                            {/* ----- images ----- */}
                            
                        </ContentBody>

                    </Container>

                    {
                        _renderConnections()
                    }        

                </Page>
                <Footer />
            </div>
        )
    }
    
}



