import React, {Component} from 'react';
import { connect } from "react-redux";
import { Link } from 'react-router-dom'
import { Container, Col, Row } from 'reactstrap'
import posed from 'react-pose'

import { refreshLoadItems } from '../../redux/reducers'

import { pages } from '../../navigation'
import { 
  HeaderContainer,
  CenterHorizontally,
  CenterVertically,
  colours,
} from '../../styles'

import MenuIcon   from  'react-icons/lib/fa/align-justify'
import CloseIcon  from  'react-icons/lib/fa/close'

const plant = require('../../asset/img/press/plant.png')
const favicon = require('../../asset/img/favicon.png')

var interval = undefined;


const MenuContainer = posed.div({
  open: { opacity: 1, y: 0, transition: { easeing: 'easeOut' }, staggerChildren: 100, delayChildren: 200 },
  closed: { opacity: 0, y: '-250px', transition: { easeing: 'easeIn' } }
})

const MenuItem = posed.div({
  open: { opacity: 1, x: 0,  },
  closed: { opacity: 0, x: '-100px',  }
})





/*

HAS TO LOAD BASED INDIVIDUAL IMAGES,

each page has to set the images that are to be downloaded, and update the top when done so (or asset even)
possibly have indpendant video or image components that do this - can manaully check

also, there should be atime limit - if an asset cant be grabbed in a certain amount of time, failsafe and it fires anyway
this should then be called and reloaded etc, only handle loading from here 

*/


class Header extends Component {

  state = {
    prevRoute: {},
    currentRoute: {},
    isLoading: true,
    loadingPage: false,
    loadPercent: 0,

    maxItems: 0, // the max amount of items that have been displayed - resets every new page

    menuOpen: false,
    menuOffScreen: true // Used so that I can delay display:none and 'closed' class with CSS 
  }

  _toggleMenu = () => {
    const { menuOpen } = this.state;

    // When the menu is being animated out - can turn display off when animation finished - so that can click underneath
    if (menuOpen === true) {
      setTimeout(() => {
        this.setState({ menuOffScreen: true })
      }, 400);
    }

    this.setState({ menuOpen: !menuOpen, menuOffScreen: false })
  }

  componentWillReceiveProps(nextProps) {
    
    const { maxItems } = this.state;
    const loadItems = nextProps.loadItems;

    var max = (loadItems > maxItems) ? loadItems : maxItems;
    var percent = 100 - ((100 / max) * loadItems) 

    if (loadItems > 0 && percent === 0) {
      percent = 1;
    }

    if (percent === 100) {
      percent = 0;
    }

    this.setState({ currentRoute: nextProps.location, menuOpen: false, menuOffScreen: true,
      loadingPage: (loadItems > 0) ? true : false,
      maxItems: (loadItems > maxItems) ? loadItems : maxItems,
      loadPercent: percent  
    }); // Also close menu when navigating elsewhere

    if (nextProps.location.pathname != this.props.location.pathname) {
      window.scrollTo(0, 0);
      this.props.refreshLoadItems();
      this.setState({prevRoute: this.props.location, maxItems: 0})
    }
  }

  componentWillMount() {

    this.setState({ currentRoute: this.props.location });

    setTimeout(()=> {
        this.setState({ isLoading: false })
      }, 500);

    return;
  }

  _makeNavButton = (page) => {

    const { currentRoute } = this.state;

    return (
        <Col key={page.path + '-' + page.title} >
          <CenterVertically>
            <CenterHorizontally>
                { (page.path === currentRoute.pathname) ?
                    <p>{page.title}</p>
                :
                    <Link to={page.path}>{page.title}</Link>
                }
            </CenterHorizontally>
          </CenterVertically>
        </Col>
    )
  }

  render() {

    const { _makeNavButton, _toggleMenu } = this;
    const { loadItems } = this.props;
    const { isLoading, loadPercent, menuOpen, menuOffScreen } = this.state;

    const backgroundFill  = (menuOpen === true || (loadPercent < 100 && loadPercent > 0)) ? true : false;
    const hiddenClass     = (loadItems > 0) ? '' : ' hidden';

    if (isLoading === true) { return null }

      return (
          <HeaderContainer background={backgroundFill}>

              <Container className="full-height non-responsive">
                  <CenterVertically>
                    <Row >
                      <Col md={5} lg={7}>
                        <CenterVertically>
                        {/* <div>FRIENDSVILLE</div> */}
                        {/*<img className="topIcon" src={plant}/>*/}
                        {/*<img className="topIcon" src={favicon}/>*/}

                        </CenterVertically>       
                      </Col>
                      <Col>
                        <Row>
                            {pages.map((page) => {
                                if (page.inNav === true) return _makeNavButton(page);
                            })}
                        </Row>
                      </Col>
                    </Row>
                  </CenterVertically>
              </Container>


              <div className="responsive">
                <Container className={"responsive-body " + ((menuOpen === true) ? 'open' : '')}>
                  
                  <CenterVertically className="full-height">
                    {/*<img className="topIcon" src={favicon}/>*/}
                  </CenterVertically>
                  
                  <div className={((menuOpen === true) ? 'selected' : '') + ' menuIcon'} onClick={_toggleMenu}>
                    {
                      (menuOpen === true) ? 
                      <CloseIcon size={30} color={colours.primary}/>
                      :
                      <MenuIcon size={30} color={colours.primary}/>
                    }       
                  </div>

                </Container>

                <MenuContainer pose={(menuOpen === true) ? 'open' : 'closed'} className={"menu " + ((menuOffScreen === true) ? 'closed' : null) }>

                  {pages.map((page) => {
                    if (page.inNav === true) return (
                      <MenuItem key={'header_menuItem_' + page.title}>{_makeNavButton(page)}</MenuItem>
                    )
                  })}

                </MenuContainer>
              </div>

              <div style={{width: loadPercent + '%'}} className={'loadingbar ' + hiddenClass }></div>
          </HeaderContainer>
      )
  }

}



const mapDispatchToProps = dispatch => {
  return {
    refreshLoadItems: () => { dispatch(refreshLoadItems()) }
  }
}

const mapStateToProps = state => {
  return { loadItems: state.loadItems };
};



export default connect(mapStateToProps, mapDispatchToProps)(Header);