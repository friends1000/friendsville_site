import React, { Component } from 'react';


import {
    LockContainerCont
} from '../../styles/index'


export default class LockContainer extends Component {

    state = {}
    interval = undefined;

    componentDidMount() {

        const { date } = this.props;

        var deadline = date.getTime(); 
        this.interval = setInterval(() => { 
            
            var now = new Date().getTime(); 
            var t = deadline - now; 
            var days = Math.floor(t / (1000 * 60 * 60 * 24)); 
            var hours = Math.floor((t%(1000 * 60 * 60 * 24))/(1000 * 60 * 60)); 
            var minutes = Math.floor((t % (1000 * 60 * 60)) / (1000 * 60)); 
            var seconds = Math.floor((t % (1000 * 60)) / 1000); 

            this.setState({ dateTime: days + "d "  
            + hours + "h " + minutes + "m " + seconds + "s " });

            if (t < 0) { 
                clearInterval(this.interval); 
                this.setState({ dateTime: 'OUT NOW!' });
            } 
        }, 1000); 
    }

    componentWillUnmount() {
        clearInterval(this.interval); 
    }


    render() {
    
        const { dateTime } = this.state;

        return (
            <div className="position-relative">
                <LockContainerCont>
                    <h1>LOCKED</h1>
                    
                    <img src={require('../../asset/img/misc/lock.png')}/>
                    <p>{dateTime}</p>
                </LockContainerCont>
            </div>
        )   
    }

}






