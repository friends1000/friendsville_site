import React from "react";
import { LazyContainer } from '../../styles'



export class LazyImage extends React.Component {



  constructor(props) {
    super(props)
    this.state = { loading: true }
  }

  onLoad = () => {

    this.setState({ loading: false })

    const { onLoad } = this.props;
    if (onLoad) { onLoad(); }
  }


  // Just render the image with data-src 
  render() {
  
    const { alt, src, lazy, sizes, width, height } = this.props;
    const { loading } = this.state;
    const { onLoad } = this;

    const lazyImage = (loading === true) ? 'lazy visible' : 'lazy hidden';
    const toLoad = (loading === true) ? 'main hidden' : 'main visible';

    return (
      <LazyContainer>
        <img src={src} alt={alt} className={toLoad} onLoad={onLoad}/>
        <img src={lazy} alt={lazy} className={lazyImage} />
      </LazyContainer>
    )
  }
}

export default LazyImage;
