import React, { Component } from 'react'
import { connect } from 'react-redux'
import { addLoadItem, removeLoadItem } from '../../redux/reducers'
import { AbsoluteCenter, colours } from '../../styles';
import ReactLoading from 'react-loading'


class LoadContainer extends Component {

    state = { loading: true }

    constructor(props) {
        super(props);

        props.addLoadItem();
    }

    _itemLoad = () => {
        this.setState({ loading: false })
        this.props.removeLoadItem();
        //setTimeout(() => this.props.removeLoadItem(), (Math.random() * 3000) + 1000 )
    }

    render() {
        const { _itemLoad } = this;
        const { children, noLoadIcon } = this.props;
        const { loading } = this.state;


        const childrenWithProps = React.Children.map(children, child => {
            return React.cloneElement(child, { onLoad: _itemLoad })
        });

        // If you dont want to display load - but want it to track at top - use this prop
        if (noLoadIcon) return childrenWithProps;

        const childClass = (loading === true) ? 'hidden' : null;

        return (
            <div className="loadContainer_container">

                <div className={childClass}>
                    {childrenWithProps}
                </div>

                {
                    (loading === true) ?
                        <AbsoluteCenter className="absolute_fill">
                            <ReactLoading height={30} type="spin" width={30} color={colours.primary}/>
                        </AbsoluteCenter>
                    :
                    null
                }
            </div>
        )
    }
}


function mapDispatchToProps(dispatch) {
    return {
        addLoadItem: () => { dispatch(addLoadItem()) },
        removeLoadItem: () => { dispatch(removeLoadItem()) }
    };
}

export default connect(null, mapDispatchToProps)(LoadContainer);