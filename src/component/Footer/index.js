import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import { Container, Row, Col } from 'reactstrap'


import { FooterContainer, colours, CenterHorizontally, CenterVertically } from '../../styles'
import { animations, FadeDiv } from '../../animations'



import Instagram    from    'react-icons/lib/fa/instagram'
import Twitter      from    'react-icons/lib/fa/twitter'
import Youtube      from    'react-icons/lib/fa/youtube-play'
import Soundcloud   from    'react-icons/lib/fa/soundcloud'
import Spotify      from    'react-icons/lib/fa/spotify'
import AppleMusic   from    'react-icons/lib/fa/apple'


const buttonSize    = 20
const colour        = colours.primary;

const buttons = [
    { comp: <Instagram size={buttonSize} color={colour} />,     link: 'https://www.instagram.com/friends1000_/' }  ,
    { comp: <Twitter size={buttonSize} color={colour} />,       link: 'https://twitter.com/friends1000_' }  ,
    { comp: <Youtube size={buttonSize} color={colour} />,       link: 'https://www.youtube.com/channel/UCZa4YLsx--1Y86VDZwC8Wmw' }  ,
    { comp: <Soundcloud size={buttonSize} color={colour} />,    link: 'https://soundcloud.com/friends1000' }  ,
    { comp: <Spotify size={buttonSize} color={colour} />,       link: 'https://open.spotify.com/artist/2nNqtIywX0GiSFzJ7M9Qhz' }  ,
    { comp: <AppleMusic size={buttonSize} color={colour} />,    link: 'https://itunes.apple.com/us/artist/friends1000/1363365486' }  ,
]   

function openLink(link) {
    window.open(link,'_blank');
}

const Footer = (props) => {

    return (
        <FadeDiv animation={animations.fadeUp} delay={1} key={'footer_container'}>
            <FooterContainer>
                <Container className="fullHeight">
                    <CenterVertically>
                        <div className="buttonContainer">
                            { buttons.map((button) => {
                                return (

                                    <div key={'footer_' + button.link} className="selectable" onClick={() => { openLink(button.link) }}>
                                        {button.comp}
                                    </div>

                                )
                            })}
                        </div>
                    </CenterVertically>
                
                </Container>
                
            </FooterContainer>
        </FadeDiv>
    )
}


/*
<CenterVertically className="bg-success">
                                        <CenterHorizontally>
</CenterHorizontally>
                                    </CenterVertically>

*/


export default Footer;
