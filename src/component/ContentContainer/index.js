import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Container, Row, Col } from 'reactstrap';
import styled /*, { injectGlobal }*/ from 'styled-components';

import { ContentBody, colours } from '../../styles';
import { animations, FadeDiv } from '../../animations';
import ContentBox from '../ContentBox';

import { getContentById } from '../../data'


const childDelay = 300;
const childStagger = 150;



const ContentContainer = (props) => {
    const { data, delay, onClick } = props;
    const { id, title, items } = data;

    function _onClick(id) {
        if (onClick != undefined) {
            onClick(id);
        }
    }

    return (

            <FadeDiv animation={animations.fadeUp} childDelay={childDelay} childStagger={childStagger} delay={delay || 1} key={'selectionbox_' + title + '-outer'}>
                <ContentBody>
                    <h3>{title}</h3>

        
                        <div className="contentContainer">
                            {items.map((item) => {
                                const dataGrab = getContentById(item.id)

                                if (dataGrab != null) {
                                    return (
                                        <Col xs={12} sm={6} md={4} lg={3} key={'content_container-' + title + '-' + item.id}>
                                            <ContentBox key={'content_container-' + item.id + '-body'} data={dataGrab} onClick={_onClick}/>
                                        </Col>
                                    )
                                }
                            })}
                        </div>
   

                </ContentBody>
            </FadeDiv>
    
    )
}





export default ContentContainer;
