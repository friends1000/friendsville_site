import React, { Component } from 'react';
import { Container, Col } from 'reactstrap'
import { ContactFormContainer, colours } from '../../styles';
import ReactLoading from 'react-loading'
import axios from 'axios'
import ReactGA from 'react-ga'

import { FadeDiv, animations } from '../../animations'

const apiUrl = 'https://friendsville-api.herokuapp.com/api/world';

const delay     =   200;
const stagger   =   100;

const fieldAnim = animations.fadeUp;

const contentTypes = { input: 'input', textarea: 'textarea' };
const contentItems = [
    {
        id: 'name',
        title: 'Name',
        base_value: '',
        type: contentTypes.input
    },
    {
        id: 'email',
        title: 'Email',
        base_value: '',
        type: contentTypes.input
    },
    {
        id: 'subject',
        title: 'Subject',
        base_value: '',
        type: contentTypes.input
    },
    {
        id: 'message',
        title: 'Message',
        base_value: '',
        type: contentTypes.textarea
    },
]



export default class ContactForm extends Component {

     
     componentWillMount() {
         this.resetFields();
     }

     resetFields = () => {
        var data = {};

        contentItems.map((item) => {
            data[item.id] = ''
        })

        this.setState( data )
     }
    

    _submitForm = () => {

        const { message } = this.state;

        if (message.length === 0) {
            this.setState({ success: false, response: 'Message can not be empty' })
            return;
        }

        this.setState({ loading: true });

        ReactGA.event({
            category: 'Submit',
            action: 'Sent message on Hotline'
        });

        axios.post(apiUrl, this.state).then((result) => {
            console.log(result)
            this.setState({ loading: false, success: true, response: 'F1K Hotline message received!' })
            this.resetFields();
        })
        .catch((err) => {
            console.log(err)
            this.setState({ loading: false, success: false, response: 'Unable to send your message, try again or hit up team@friendsville.co'})
        })
    }

    render() {

        const { _submitForm } = this;
        const { loading, success, name, email, subject, message, response } = this.state;

        return (
            <ContactFormContainer>

                <FadeDiv animation={fieldAnim} childDelay={delay} childStagger={stagger} delay={100}>
                <Container className="body" fluid={true}>
                    {
                        contentItems.map((item) => {
                            
                            return (
                                <FadeDiv animation={fieldAnim} key={'contact_form_field-' + item.id}>
                                    <Col>
                                        <label className={(this.state[item.id].length > 0) ? 'bold' : null}>{item.title}</label>

                                        {
                                            (item.type === contentTypes.input) ?
                                            <input value={this.state[item.id]} onChange={(val) => { this.setState({ [item.id]: val.target.value })}}/>
                                            :
                                            <textarea value={this.state[item.id]} onChange={(val) => { this.setState({ [item.id]: val.target.value })}}/>
                                        }
                                        
                                    </Col>
                                </FadeDiv>
                            )
                        })
                    }
                    
                    <FadeDiv animation={fieldAnim}>
                    <Col>
                        <div className="buttonContainer selectable">
                            {
                                (loading === true) ?
                                <ReactLoading type={'spin'} height={25} width={25} color={'white'} />
                                :
                                <a onClick={_submitForm}>Submit</a>
                            }
                        </div>
                    </Col>
                    </FadeDiv>

                    <p className={(success === true) ? 'success' : 'fail'}>{response}</p>

                </Container>
                </FadeDiv>

            </ContactFormContainer>
        );
        
        
    }
}




