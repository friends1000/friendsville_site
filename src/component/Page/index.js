import React, { Component } from 'react';
import {connect} from 'react-redux'
import {
    colours,
    Page as PageContainer
} from '../../styles'
import { FadeDiv } from '../../animations'



class Page extends Component {

    state = { loading: false }

    render() {
        const { props, state } = this;
        const { loading } = state;
        const { children } = props;

        const hidden = (loading === true) ? 'hidden' : null;

        return (
            <PageContainer {...props} className={hidden}>
                {children}
            </PageContainer>
        )
    }

}


const mapStateToProps = state => {
    return { loadItems: state.loadItems };
  };
  
  
  
  export default connect(mapStateToProps)(Page);