import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import { Container, Row, Col } from 'reactstrap'
import styled /*, { injectGlobal }*/ from 'styled-components';

import { SelectionBox as SelectionContainer, ContentImage } from '../../styles'
import { animations, FadeDiv } from '../../animations'
import LoadContainer    from    '../LoadContainer';
import LazyImage        from    '../LazyImage'
import { LazyLoadImage } from 'react-lazy-load-image-component';

const childDelay = 200;
const childStagger = 50;



const mainWithHover = { 
    ...animations.fadeUp,
    end: { ...animations.fadeUp.end, scale: 1 },
    hoverable: true,
    hover: {
        scale: 1.05
    }
}


const childWithHover = {
    ...animations.fadeRight,
    /*
    hover: {
        //y: -150,
        paddingBottom: 150
    },
    end: { ...animations.fadeRight.end, scale: 1, x: 0, y: 0, paddingBottom: 0 }
    */
}





const SelectionBox = (props) => {
    const { data, delay, onClick } = props;
    const { id, title, image, description, imageComp } = data;

    function _onClick() {
        if (onClick) {
            onClick(id);
        }
    }

    return (

            <FadeDiv animation={mainWithHover} childDelay={childDelay} childStagger={childStagger} delay={delay} key={'selectionbox_' + title + '-outer'}>

                <div className="position-relative">
                    <SelectionContainer onClick={_onClick}>
                        <div className="sectionBody">
                            <FadeDiv animation={animations.fadeUp} key={'selectionbox_' + title + '-inner'}>
                                <Container fluid={true}>

                                    <Row> 
                                        <ContentImage>
                                            <LoadContainer noLoadIcon={true}>
                                                <LazyLoadImage src={image} alt={title + '_img'} width={300} height={250}/>                                                                
                                            </LoadContainer>
                                        </ContentImage>
                                    </Row>

                                </Container>
                            </FadeDiv>
                        </div>

                        <div className="details">
                            <FadeDiv animation={childWithHover}>
                    
                                <div>
                                    <h4>{title}</h4>
                                </div>
                                {/*<div>
                                    <p>{description}</p>
                                </div>*/}
                    
                            </FadeDiv>
                    </div>
                    </SelectionContainer>
                </div>
            </FadeDiv>
    
    )
}





export default SelectionBox;
