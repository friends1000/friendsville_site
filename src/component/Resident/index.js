import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import { Container, Row, Col } from 'reactstrap'

import { LazyLoadImage } from 'react-lazy-load-image-component';

import posed from 'react-pose'

import { ResidentContainer, colours, CenterHorizontally, CenterVertically, ContentBody } from '../../styles'
import { animations, FadeDiv } from '../../animations'

import ReactGA from 'react-ga'

/*
import Instagram    from    'react-icons/lib/fa/instagram'
import Twitter      from    'react-icons/lib/fa/twitter'
import Youtube      from    'react-icons/lib/fa/youtube-play'
import Soundcloud   from    'react-icons/lib/fa/soundcloud'
import Spotify      from    'react-icons/lib/fa/spotify'
import AppleMusic   from    'react-icons/lib/fa/apple'
*/

import Info             from    'react-icons/lib/fa/info-circle'
import Close            from    'react-icons/lib/fa/close'
import LoadContainer    from    '../LoadContainer';
import LazyImage        from    '../LazyImage';


const iconSize      = 40
const iconColour    = colours.primary;
 

function openLink(link) {
    window.open(link,'_blank');
}

const DetailsBox = posed.div({
    open: { opacity: 1, x: 0, y: 0, scale: 1, delayChildren: 300, staggerChildren: 100 },
    closed: { opacity: 0, x: -5, y: -5, scale: 0.2 }
})

const StatsContainer = posed.div({
    closed: { opacity: 0, y: -25 },
    open: { opacity: 1, y: 0, transition: { easeing: 'linear' }, delayChildren: 200, staggerChildren: 100 }
})

const StatBar = posed.div({
    closed: { width: '1%' },
    open: { width: '100%' }
})



export default class Resident extends Component {

    state = {
        detailsOpen: false
    }

    _showData = () => {
        const { detailsOpen } = this.state;
        const { data } = this.props;
        const { name } = data;

        if (detailsOpen === false) {
            
            ReactGA.event({
                category: 'View',
                action: 'view: ' + name
            });
        }
        
        this.setState({ detailsOpen: !detailsOpen })
    }

    render() {
        const { _showData } = this;
        const { detailsOpen } = this.state;
        const { data, delay } = this.props;
        const { image, name, quote, description } = data;
        const stats = data.data;

        return (
            <FadeDiv animation={animations.fadeUp} delay={delay || 1} childDelay={500} key={'footer_container'}>
                <ResidentContainer>

                    <FadeDiv animation={animations.fadeUp}>
                        <div className="info" onClick={_showData}>
                            {
                                (detailsOpen === true) ?
                                <Close size={iconSize} color={iconColour}/>
                                :
                                <Info size={iconSize} color={iconColour}/>
                            }
                            
                        </div>
                   </FadeDiv>
                    
                    {/*<LoadContainer>
                            <img src={image}/>
                    </LoadContainer>*/}
                    <LazyLoadImage alt={name + '_image'} src={image} />

                    <div className="details-container">
                        <DetailsBox pose={(detailsOpen === true) ? 'open' : 'closed'} >
                            <ContentBody background={colours.colorB} roundedTop noBottom >
                                <h5>{name}</h5>
                                <h6>"{quote}"</h6>
                                <p>{description}</p>

                            </ContentBody>

                           {/* <StatsContainer> */}
                            
                            <ContentBody roundedBottom noTop background={colours.colorA} >

                                <div className="statContainer">
                                    {
                                        stats.map((stat) => {
                                            return (
                                       
                                                <div className="stat" key={stat.key + '_for-resident-' + name}>

                                                    <div className="key">
                                                        <p>{stat.key}</p>
                                                    </div>

                                                    <div className="value">
                                                        <p>{stat.value}%</p>
                                                    </div>

                                                    <div className="bar-container">
                                                        <StatBar>
                                                        <div className="statbar" style={{width: stat.value + '%'}}></div>
                                                        </StatBar>
                                                    </div>
                                                </div>
                                             
                                            )
                                        })
                                    }                                    
                                </div>
                            </ContentBody>

                            {/* </StatsContainer> */}

                        </DetailsBox>
                    </div>

                </ResidentContainer>
            </FadeDiv>
        )
    }
    
}



